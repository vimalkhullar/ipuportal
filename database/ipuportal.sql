-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2015 at 11:51 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ipuportal`
--

-- --------------------------------------------------------

--
-- Table structure for table `college`
--

CREATE TABLE IF NOT EXISTS `college` (
  `co_id` int(3) NOT NULL,
  `co_name` varchar(50) NOT NULL,
  `co_contact` longtext NOT NULL,
  `img1` longtext NOT NULL,
  `MCA` tinyint(1) NOT NULL DEFAULT '0',
  `BCA` tinyint(1) NOT NULL DEFAULT '0',
  `BTECH` tinyint(1) NOT NULL DEFAULT '0',
  `MBA` tinyint(1) NOT NULL DEFAULT '0',
  `MTECH` tinyint(1) NOT NULL DEFAULT '0',
  `test` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`co_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `college`
--

INSERT INTO `college` (`co_id`, `co_name`, `co_contact`, `img1`, `MCA`, `BCA`, `BTECH`, `MBA`, `MTECH`, `test`) VALUES
(25, 'VIPS', '999999999', '', 1, 1, 0, 1, 0, 0),
(47, 'USIT', '999999999', '', 1, 0, 0, 1, 0, 0),
(55, 'BVICAM', '999999999', '', 1, 0, 0, 0, 0, 0),
(77, 'MAIT', '999999999', '', 1, 0, 0, 1, 0, 0),
(85, 'JIMS', '999999999', '', 1, 1, 0, 1, 0, 0),
(101, 'testcol', '12345', '', 0, 0, 0, 1, 0, 1),
(556, 'AIT', '56456', '', 0, 0, 0, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `c_id` int(3) NOT NULL,
  `c_name` varchar(20) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`c_id`, `c_name`) VALUES
(65, 'MBA'),
(67, 'BTECH'),
(82, 'BCA'),
(87, 'MTECH'),
(92, 'MCA'),
(101, 'test');

-- --------------------------------------------------------

--
-- Table structure for table `rank`
--

CREATE TABLE IF NOT EXISTS `rank` (
  `rco_id` int(3) NOT NULL,
  `GEN` int(5) NOT NULL,
  `rc_name` varchar(20) NOT NULL,
  `rco_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rank`
--

INSERT INTO `rank` (`rco_id`, `GEN`, `rc_name`, `rco_name`) VALUES
(55, 200, 'MCA', 'BVICAM'),
(47, 99999, 'MBA', 'USIT'),
(47, 100, 'MCA', 'USIT'),
(25, 99999, 'MBA', 'VIPS'),
(25, 99999, 'BCA', 'VIPS'),
(25, 500, 'MCA', 'VIPS'),
(85, 99999, 'MBA', 'JIMS'),
(85, 99999, 'BCA', 'JIMS'),
(85, 500, 'MCA', 'JIMS'),
(77, 99999, 'MBA', 'MAIT'),
(77, 99999, 'MCA', 'MAIT'),
(556, 99999, 'MTECH', 'AIT'),
(101, 89, 'MBA', 'testcol'),
(101, 99999, 'test', 'testcol');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
